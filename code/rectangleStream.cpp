#include <iostream>

using namespace std;

class rectangleType
{
	//Overload the stream insertion and extraction operators
	friend ostream& operator << (ostream&, const rectangleType &);
	friend istream& operator >> (istream&, rectangleType &);

public:
	void setDimension(double l, double w);
	double getLength() const;
	double getWidth() const;
	double area() const;
	double perimeter() const;
	void print() const;

	rectangleType operator+(const rectangleType&) const;
	//Overload the operator +
	rectangleType operator*(const rectangleType&) const;
	//Overload the operator *

	bool operator==(const rectangleType&) const;
	//Overload the operator ==
	bool operator!=(const rectangleType&) const;
	//Overload the operator !=

	rectangleType();
	rectangleType(double l, double w);

private:
	double length;
	double width;
};


void rectangleType::setDimension(double l, double w)
{
	if (l >= 0)
		length = l;
	else
		length = 0;

	if (w >= 0)
		width = w;
	else
		width = 0;
}

double rectangleType::getLength() const
{
	return length;
}

double rectangleType::getWidth()const
{
	return width;
}

double rectangleType::area() const
{
	return length * width;
}

double rectangleType::perimeter() const
{
	return 2 * (length + width);
}

void rectangleType::print() const
{
	cout << "Length = " << length
		<< "; Width = " << width;
}

rectangleType::rectangleType(double l, double w)
{
	setDimension(l, w);
}

rectangleType::rectangleType()
{
	length = 0;
	width = 0;
}
ostream& operator << (ostream& osObj, const rectangleType & rectangle)
{
	osObj << "Lenght = " << rectangle.length
		<< "width = " << rectangle.width;

	return osObj;
}
istream& operator >> (istream& isObj, rectangleType & rectangle)
{
	isObj >> rectangle.length >> rectangle.width;

	return isObj;
}

int main()
{
	rectangleType myRectangle(23, 45);
	rectangleType yourRectangle;

	cout << "myRectangle : " << myRectangle << endl;

	cout << "Enter the length and width "
		<< "of a rectangle : " << endl;
	
	cin >> yourRectangle;

	cout << "yourRectangle : " << yourRectangle << endl;


	return 0;
}