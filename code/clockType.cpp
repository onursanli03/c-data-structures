#include <iostream>

using namespace std;

class clockType
{
	friend ostream& operator<<(ostream&, const clockType&);
	friend istream& operator>>(istream&, clockType&);

public:
	void setTime(int hours, int minutes, int seconds);
	//Function to set the member variables hr, min, and sec.  
	//Postcondition: hr = hours; min = minutes; sec = seconds

	void getTime(int& hours, int& minutes, int& seconds) const;
	//Function to return the time.
	//Postcondition: hours = hr; minutes = min; seconds = sec

	clockType operator++();
	//Overload the pre-increment operator.
	//Postcondition: The time is incremented by one second.

	clockType operator++(int x);

	bool operator==(const clockType& otherClock) const;
	//Overload the equality operator.
	//Postcondition: Returns true if the time of this clock 
	//               is equal to the time of otherClock,
	//               otherwise it returns false.

	bool operator!=(const clockType& otherClock) const;
	//Overload the not equal operator.
	//Postcondition: Returns true if the time of this clock 
	//               is not equal to the time of otherClock,
	//               otherwise it returns false.

	bool operator<=(const clockType& otherClock) const;
	//Overload the less than or equal to operator.
	//Postcondition: Returns true if the time of this clock
	//               is less than or equal to the time of 
	//               otherClock, otherwise it returns false.

	bool operator<(const clockType& otherClock) const;
	//Overload the less than operator.
	//Postcondition: Returns true if the time of this clock
	//               is less than the time of otherClock,
	//               otherwise it returns false.

	bool operator>=(const clockType& otherClock) const;
	//Overload the greater than or equal to operator.
	//Postcondition: Returns true if the time of this clock
	//               is greater than or equal to the time of 
	//               otherClock, otherwise it returns false.

	bool operator>(const clockType& otherClock) const;
	//Overload the greater than operator.
	//Postcondition: Returns true if the time of this clock
	//               is greater than the time of otherClock,
	//               otherwise it returns false.

	clockType(int hours = 0, int minutes = 0, int seconds = 0);
	//Constructor to initialize the object with the values 
	//specified by the user. If no values are specified,
	//the default values are assumed.
	//Postcondition: hr = hours; min = minutes; 
	//               sec = seconds;


private:
	int hr;  //variable to store the hours
	int min; //variable to store the minutes
	int sec; //variable to store the seconds
};



void clockType::setTime(int hours, int minutes, int seconds)
{
	if (0 <= hours && hours < 24)
		hr = hours;
	else
		hr = 0;

	if (0 <= minutes && minutes < 60)
		min = minutes;
	else
		min = 0;

	if (0 <= seconds && seconds < 60)
		sec = seconds;
	else
		sec = 0;
}

void clockType::getTime(int& hours, int& minutes,
	int& seconds) const
{
	hours = hr;
	minutes = min;
	seconds = sec;
}

//Constructor
clockType::clockType(int hours, int minutes, int seconds)
{
	setTime(hours, minutes, seconds);
}


//Overload the equality operator.
bool clockType::operator==(const clockType& otherClock) const
{
	return (hr == otherClock.hr && min == otherClock.min
		&& sec == otherClock.sec);
}

//Overload the not equal operator.
bool clockType::operator!=(const clockType& otherClock) const
{
	return (hr != otherClock.hr || min != otherClock.min
		|| sec != otherClock.sec);
}
clockType clockType::operator++()
{
	sec++;

	if (sec > 59)
	{
		sec = 0;
		min++;
		if (min > 59)
		{
			min = 0;
			hr++;
			if (hr > 23)
			{
				hr = 0;
			}

		}
	}

	return *this;
}

clockType clockType::operator++(int x)
{
	clockType temp = *this; 
	sec++;

	if (sec > 59)
	{
		sec = 0;
		min++;
		if (min > 59)
		{
			min = 0;
			hr++;
			if (hr > 23)
			{
				hr = 0;
			}

		}
	}
	return temp;
}

bool clockType::operator<=(const clockType& otherClock) const
{

	return ((hr < otherClock.hr) || (hr == otherClock.hr && min < otherClock.min) || (hr == otherClock.hr && min ==otherClock.min && sec <= otherClock.sec));

}
bool clockType::operator<(const clockType& otherClock) const
{

	return ((hr < otherClock.hr) || (hr == otherClock.hr && min < otherClock.min) || (hr == otherClock.hr && min == otherClock.min && sec < otherClock.sec));

}
bool clockType::operator>(const clockType& otherClock) const
{

	return ((hr > otherClock.hr) || (hr == otherClock.hr && min > otherClock.min) || (hr == otherClock.hr && min == otherClock.min && sec > otherClock.sec));

}
bool clockType::operator>=(const clockType& otherClock) const
{

	return ((hr > otherClock.hr) || (hr == otherClock.hr && min > otherClock.min) || (hr == otherClock.hr && min == otherClock.min && sec >= otherClock.sec));

}
ostream& operator<<(ostream& osObject, const clockType& timeOut) 
{
	if (timeOut.hr < 10)
	{
		osObject << "0";
		osObject << timeOut.hr << " : ";
	}
	if (timeOut.min < 10)
	{
		osObject << "0";
		osObject << timeOut.min << " : ";
	}
	if (timeOut.sec < 10)
	{
		osObject << "0";
		osObject << timeOut.sec << " : ";
	}

	return osObject;
}
istream& operator>>(istream& isObject, clockType& timeIn)
{
	char ch;

	isObject >> timeIn.hr;

	if (timeIn.hr < 0 || timeIn.hr >= 24)
	{
		timeIn.hr = 0;
	}

	isObject.get(ch); //read and discard

	isObject >> timeIn.min;

	if (timeIn.min < 0 || timeIn.min >= 60)
	{
		timeIn.min = 0;
	}
	isObject.get(ch); //read and discard

	isObject >> timeIn.sec;

	if (timeIn.sec < 0 || timeIn.sec >= 60)
	{
		timeIn.sec = 0;
	}
	return isObject;
}

int main()
{
	clockType myClock(5, 6, 23);
	clockType yourClock;

	cout << "my clock = " << myClock
		<< endl;

	cout << "your clock = " << yourClock
		<< endl;


	cout << " Enter the time in the form!! "
		<< "hr : min : sec";

	cin >> myClock;

	cout << "my clock = " << myClock
		<< endl;

	++myClock;

	cout << "After incrementing the time, "
		<< "myClock" << myClock << endl;

	yourClock.setTime(13, 35, 38);

	cout << "After setting the time"
		<< "yourClock" << yourClock << endl;

	if (myClock == yourClock)
	{
		cout << "The times of myClock and "
			<< " yourClock are equal. " << endl;
	}
	else
	{
		cout << "The times of myClock and "
			<< " yourClock are NOT equal. " << endl;
	}

	yourClock = myClock++;

	cout << "my clock = " << myClock
		<< endl;

	cout << "your clock = " << yourClock
		<< endl;


	return 0;
}