#include <iostream>

using namespace std;


class cAssignmentOprOverload
{
public:
	const cAssignmentOprOverload&
		operator=(const cAssignmentOprOverload& otherList);
	//Overload assignment operator

	void print() const;
	//Function to print the list

	void insertEnd(int item);
	//Function to insert an item at the end of the list
	//Postcondition: if the list is not full, 
	//                  length++; list[length] = item;
	//               if the list is full,
	//                  output an appropriate message
	void destroyList();
	//Function to destroy the list
	//Postcondition: length = 0; maxSize = 0; list = NULL;

	cAssignmentOprOverload(int size = 0);
	//Constructor
	//Postcondition: length = 0; maxSize = size; 
	//               list is an arry of size maxSize

private:
	int maxSize;
	int length;
	int *list;
};



void cAssignmentOprOverload::print() const
{
	if (length == 0)
		cout << "The list is empty." << endl;
	else
	{
		for (int i = 0; i < length; i++)
			cout << list[i] << " ";
		cout << endl;
	}
}

void cAssignmentOprOverload::insertEnd(int item)
{
	if (length == maxSize)
		cout << "List is full" << endl;
	else
		list[length++] = item;
}
void cAssignmentOprOverload::destroyList()
{
	delete[] list;
	list = NULL;
	length = 0;
	maxSize = 0;

}
cAssignmentOprOverload::cAssignmentOprOverload(int size)
{
	maxSize = size;
	length = 0;

	if (maxSize == 0)
	{
		list = NULL;
	}
	else {
		list = new int[maxSize];
	}
}
const cAssignmentOprOverload&
cAssignmentOprOverload::operator=(const cAssignmentOprOverload& otherList)
{
	if (this != &otherList)
	{
		delete[] list;
		maxSize = otherList.maxSize;
		length = otherList.length;


		list = new int[maxSize];

		for (int i = 0; i < length; i++)
		{
			list[i] = otherList.list;
		}
	}

	return *this;
}


int main()
{
	cAssignmentOprOverload intList1(10);
	cAssignmentOprOverload intList2;
	cAssignmentOprOverload intList3;
	
	
	int i;
	int number;

	cout << "Enter 5 integers";

	for (int i = 0; i < 5; i++)
	{
		cin >> number;
		intList1.insertEnd(number);
	}

	cout << endl;
	cout << "intList1 : ";
	intList1.print();

	intList3 = intList2 = intList1;

	cout << endl;
	cout << "intList2 : ";
	intList2.print();
	
	cout << endl;
	cout << "intList3 : ";
	intList3.print();
	
	intList2.destroyList();

	cout << endl;
	cout << "intList2 : ";
	intList2.print();
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	return 0;
}