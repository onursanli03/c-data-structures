#include <iostream> 

using namespace std;

class rectangleType
{
public:
	void setDimension(double l, double w);
	double getLength() const;
	double getWidth() const;
	double area() const;
	double perimeter() const;
	void print() const;

	rectangleType operator+(const rectangleType&) const;
	//Overload the operator +
	rectangleType operator*(const rectangleType&) const;
	//Overload the operator *

	bool operator==(const rectangleType&) const;
	//Overload the operator ==
	bool operator!=(const rectangleType&) const;
	//Overload the operator !=

	rectangleType();
	rectangleType(double l, double w);

private:
	double length;
	double width;
};


void rectangleType::setDimension(double l, double w)
{
	if (l >= 0)
		length = l;
	else
		length = 0;

	if (w >= 0)
		width = w;
	else
		width = 0;
}

double rectangleType::getLength() const
{
	return length;
}

double rectangleType::getWidth()const
{
	return width;
}

double rectangleType::area() const
{
	return length * width;
}

double rectangleType::perimeter() const
{
	return 2 * (length + width);
}

void rectangleType::print() const
{
	cout << "Length = " << length
		<< "; Width = " << width;
}

rectangleType::rectangleType(double l, double w)
{
	setDimension(l, w);
}

rectangleType::rectangleType()
{
	length = 0;
	width = 0;
}

rectangleType rectangleType::operator+(const rectangleType& rectangle)
{
	rectangleType tempRect;

	tempRect.length = length + rectangle.length;
	tempRect.width = width + rectangle.width;

	return tempRect;

}
rectangleType rectangleType::operator*(const rectangleType& rectangle)
{
	rectangleType tempRect;

	tempRect.length = length * rectangle.length;
	tempRect.width = width * rectangle.width;

	return tempRect;

}
bool rectangleType::operator==(const rectangleType& rectangle) const
{
	return(length == rectangle.length && width == rectangle.width);
}
bool rectangleType::operator!=(const rectangleType& rectangle) const
{
	return(length != rectangle.length || width != rectangle.width);
}


int main()
{
	rectangleType rectangle1(23, 45);
	rectangleType rectangle2(12, 10);
	rectangleType rectangle3;
	rectangleType rectangle4;

	cout << " rectangle1 : ";
	rectangle1.print();
	cout << endl;

	cout << "rectangle2 : ";
	rectangle2.print();
	cout << endl;
	
	cout << " rectangle3 : ";
	rectangle3.print();
	cout << endl;
	
	cout << " rectangle4 : ";
	rectangle4.print();
	cout << endl;

	rectangle3 = rectangle1 + rectangle2;
	
	cout << " rectangle3 : ";
	rectangle3.print();
	cout << endl;

	rectangle4 = rectangle1 * rectangle2;

	cout << " rectangle4 : ";
	rectangle4.print();
	cout << endl;
	
	system("PAUSE");

	if (rectangle1 == rectangle2)
	{
		cout << "rectangle1 and rectangle2 are equal" << endl;
	}
	else {
		cout << "rectangle1 and rectangle2 are not equal" << endl;
	}
	
	if (rectangle1 != rectangle3)
	{
		cout << "rectangle1 and rectangle3 are not equal" << endl;
	}
	else {
		cout << "rectangle1 and rectangle3 are equal" << endl;
	}

	return 0;
}