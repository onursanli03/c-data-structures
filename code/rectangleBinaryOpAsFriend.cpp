#include <iostream>

using namespace std;

class rectangleType
{
	friend rectangleType operator+(const rectangleType&,
		const rectangleType&);
	//Overload the operator +
	friend rectangleType operator*(const rectangleType&,
		const rectangleType&);
	//Overload the operator *

	friend bool operator==(const rectangleType&,
		const rectangleType&);
	//Overload the operator ==
	friend bool operator!=(const rectangleType&,
		const rectangleType&);
	//Overload the operator !=
public:
	void setDimension(double l, double w);
	double getLength() const;
	double getWidth() const;
	double area() const;
	double perimeter() const;
	void print() const;

	rectangleType();
	rectangleType(double l, double w);

private:
	double length;
	double width;
};

void rectangleType::setDimension(double l, double w)
{
	if (l >= 0)
		length = l;
	else
		length = 0;

	if (w >= 0)
		width = w;
	else
		width = 0;
}

double rectangleType::getLength() const
{
	return length;
}

double rectangleType::getWidth()const
{
	return width;
}

double rectangleType::area() const
{
	return length * width;
}

double rectangleType::perimeter() const
{
	return 2 * (length + width);
}

void rectangleType::print() const
{
	cout << "Length = " << length
		<< "; Width = " << width;
}

rectangleType::rectangleType(double l, double w)
{
	setDimension(l, w);
}

rectangleType::rectangleType()
{
	length = 0;
	width = 0;
}
rectangleType operator+(const rectangleType& firstRec,
	const rectangleType& secondRec) 
{
	rectangleType tempRec;

	tempRec.length = firstRec.length + secondRec.length;
	tempRec.width = firstRec.width + secondRec.width;

	return tempRec;

}

rectangleType operator*(const rectangleType& firstRec,
	const rectangleType& secondRec)
{
	rectangleType tempRec;

	tempRec.length = firstRec.length * secondRec.length;
	tempRec.width = firstRec.width * secondRec.width;

	return tempRec;

}

bool operator==(const rectangleType& firstRec,
	const rectangleType& secondRec)
{
	return(firstRec.length == secondRec.length && firstRec.width == secondRec.width);
}

bool operator!=(const rectangleType& firstRec,
	const rectangleType& secondRec)
{
	return(firstRec.length != secondRec.length || firstRec.width != secondRec.width);
}
int main()
{


	return 0;
}