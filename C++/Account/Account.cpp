#include <iostream>

using namespace std;

class Account{
	long   accno;
	short  acctype; // 0:dated 1: undated
	double cash;
public:
	Account(){   // default constructor
		cash = 0;
	};
	double getCash(){
		return cash;
	};
	double deposit(double _cash);
	double withdraw(double _cash);

};

double Account::deposit(double _cash)
{
	return cash += _cash;
}

double Account::withdraw(double _cash)
{
	if (_cash > cash){
		cout << " Not enough money!... You have " << cash << " cash" << endl;
		return cash;
	}
	return cash -= _cash;
}


int main()
{

	Account acc;

	cout << "Cash =" << acc.getCash() << endl;
	cout << "Cash =" << acc.deposit(100) << endl;
	cout << "Cash =" << acc.withdraw(30) << endl;

	system("pause");
	return 0;
}