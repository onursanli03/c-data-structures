#include <iostream>

using namespace std;

class rectangleType
{
	//Overload the stream insertion and extraction operators
	friend ostream& operator << (ostream&, const rectangleType &);
	friend istream& operator >> (istream&, rectangleType &);

public:
	void setDimension(double l, double w);
	double getLength() const;
	double getWidth() const;
	double area() const;
	double perimeter() const;
	void print() const;

	rectangleType operator+(const rectangleType&) const;
	//Overload the operator +
	rectangleType operator*(const rectangleType&) const;
	//Overload the operator *

	bool operator==(const rectangleType&) const;
	//Overload the operator ==
	bool operator!=(const rectangleType&) const;
	//Overload the operator !=

	rectangleType();
	rectangleType(double l, double w);

private:
	double length;
	double width;
};


void rectangleType::setDimension(double l, double w)
{
	if (l >= 0)
		length = l;
	else
		length = 0;

	if (w >= 0)
		width = w;
	else
		width = 0;
}

double rectangleType::getLength() const
{
	return length;
}

double rectangleType::getWidth()const
{
	return width;
}

double rectangleType::area() const
{
	return length * width;
}

double rectangleType::perimeter() const
{
	return 2 * (length + width);
}

void rectangleType::print() const
{
	cout << "Length = " << length
		<< "; Width = " << width;
}

rectangleType::rectangleType(double l, double w)
{
	setDimension(l, w);
}

rectangleType::rectangleType()
{
	length = 0;
	width = 0;
}

rectangleType rectangleType::operator+
(const rectangleType& rectangle) const
{
	rectangleType tempRect;

	tempRect.length = length + rectangle.length;
	tempRect.width = width + rectangle.width;

	return tempRect;
}

rectangleType rectangleType::operator*
(const rectangleType& rectangle) const
{
	rectangleType tempRect;

	tempRect.length = length * rectangle.length;
	tempRect.width = width * rectangle.width;

	return tempRect;
}

bool rectangleType::operator==
(const rectangleType& rectangle) const
{
	return (length == rectangle.length &&
		width == rectangle.width);
}

bool rectangleType::operator!=
(const rectangleType& rectangle) const
{
	return (length != rectangle.length ||
		width != rectangle.width);
}

ostream& operator << (ostream& osObject,
	const rectangleType& rectangle)
{
	osObject << "Length = " << rectangle.length
		<< "; Width = " << rectangle.width;

	return osObject;
}

istream& operator >> (istream& isObject,
	rectangleType& rectangle)
{
	isObject >> rectangle.length >> rectangle.width;

	return isObject;
}

int main()
{
	rectangleType myRectangle(23, 45);                //Line 1
	rectangleType yourRectangle;                      //Line 2

	cout << "Line 3: myRectangle: " << myRectangle
		<< endl;                                     //Line 3

	cout << "Line 4: Enter the length and width "
		<< "of a rectangle: ";                        //Line 4
	cin >> yourRectangle;                             //Line 5
	cout << endl;                                     //Line 6

	cout << "Line 7: yourRectangle: "
		<< yourRectangle << endl;                    //Line 7

	cout << "Line 8: myRectangle + yourRectangle: "
		<< myRectangle + yourRectangle << endl;      //Line 8
	cout << "Line 9: myRectangle * yourRectangle: "
		<< myRectangle * yourRectangle << endl;      //Line 9

	return 0;
}