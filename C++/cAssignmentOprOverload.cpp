#include <iostream>

using namespace std;


class cAssignmentOprOverload
{
public:
	const cAssignmentOprOverload&
		operator=(const cAssignmentOprOverload& otherList);
	//Overload assignment operator

	void print() const;
	//Function to print the list

	void insertEnd(int item);
	//Function to insert an item at the end of the list
	//Postcondition: if the list is not full, 
	//                  length++; list[length] = item;
	//               if the list is full,
	//                  output an appropriate message
	void destroyList();
	//Function to destroy the list
	//Postcondition: length = 0; maxSize = 0; list = NULL;

	cAssignmentOprOverload(int size = 0);
	//Constructor
	//Postcondition: length = 0; maxSize = size; 
	//               list is an arry of size maxSize

private:
	int maxSize;
	int length;
	int *list;
};



void cAssignmentOprOverload::print() const
{
	if (length == 0)
		cout << "The list is empty." << endl;
	else
	{
		for (int i = 0; i < length; i++)
			cout << list[i] << " ";
		cout << endl;
	}
}

void cAssignmentOprOverload::insertEnd(int item)
{
	if (length == maxSize)
		cout << "List is full" << endl;
	else
		list[length++] = item;
}

void cAssignmentOprOverload::destroyList()
{
	delete[] list;
	list = NULL;
	length = 0;
	maxSize = 0;
}

cAssignmentOprOverload::cAssignmentOprOverload(int size)
{
	maxSize = size;
	length = 0;

	if (maxSize == 0)
		list = NULL;
	else
		list = new int[maxSize];
}

const cAssignmentOprOverload& cAssignmentOprOverload::operator=
(const cAssignmentOprOverload& otherList)
{
	if (this != &otherList)  //avoid self-assignment; Line 1
	{
		delete[] list;                             //Line 2
		maxSize = otherList.maxSize;                //Line 3
		length = otherList.length;                  //Line 4

		list = new int[maxSize];                    //Line 5

		for (int i = 0; i < length; i++)            //Line 6
			list[i] = otherList.list[i];            //Line 7
	}

	return *this;                                   //Line 8
}


int main()
{
	cAssignmentOprOverload intList1(10);             //Line 9
	cAssignmentOprOverload intList2;                 //Line 10
	cAssignmentOprOverload intList3;                 //Line 11

	int i;                                           //Line 12
	int number;                                      //Line 13

	cout << "Line 14: Enter 5 integers: ";           //Line 14

	for (i = 0; i < 5; i++)                          //Line 15
	{
		cin >> number;                               //Line 16
		intList1.insertEnd(number);                  //Line 17
	}

	cout << endl;                                    //Line 18
	cout << "Line 19: intList1: ";                   //Line 19
	intList1.print();                                //Line 20

	intList3 = intList2 = intList1;                  //Line 21

	cout << "Line 22: intList2: ";                   //Line 22
	intList2.print();                                //Line 23

	intList2.destroyList();                          //Line 24

	cout << endl;                                    //Line 25
	cout << "Line 26: intList2: ";                   //Line 26
	intList2.print();                                //Line 27

	cout << "Line 28: After destroying intList2, "
		<< "intList1: ";                            //Line 28
	intList1.print();                                //Line 29

	cout << "Line 30: After destroying intList2, "
		<< "intList3: ";                            //Line 30
	intList3.print();                                //Line 31
	cout << endl;                                    //Line 32

	return 0;
}