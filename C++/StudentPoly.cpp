#include<iostream>

using namespace std;

class Student{
	char *name;
	int courseNum;
	double * grades;
public:
	Student(char* _name, int cNum);
	Student(const Student & _s);
	virtual ~Student();
	virtual void display();
};

Student::Student(char* _name, int cNum)
{
	cout << "Student: constructor()" << endl;
	name = new char[strlen(_name) + 1];
	strcpy(name, _name);

	courseNum = cNum;

	grades = new double[courseNum];

	for (int i = 0; i < courseNum; i++)
		grades[i] = 0.0;

}

Student::Student(const Student & _s)
{
	cout << "Student: Copy constructor()" << endl;
	name = new char[strlen(_s.name) + 1];
	strcpy(name, _s.name);

	courseNum = _s.courseNum;

	grades = new double[courseNum];

	for (int i = 0; i < courseNum; i++)
		grades[i] = _s.grades[i];
}

Student::~Student()
{
	cout << "Student's destructor..." << endl;
	delete[] name;
	delete[] grades;
}

void Student::display()
{
	cout << endl;
	cout << "Student's display.." << endl;
	cout << "Name: " << name << endl;
	cout << "Course number: " << courseNum << endl;
	cout << "Grades: " << endl;

	for (int j = 0; j < courseNum; j++)
		cout << grades[j] << endl;
}

class Assistant : public Student{

	double salary;
public:
	Assistant(char * name, int _cNum, double _salary);
	Assistant(const Assistant & _a);
	void display();
	~Assistant();
};

Assistant::Assistant(char * _name, int _cNum, double _salary)
	:Student(_name, _cNum)
{
	salary = _salary;
}

Assistant::Assistant(const Assistant & _a) : Student(_a)
{
	salary = _a.salary;
}

Assistant::~Assistant()
{
	cout << "Assistant's destructor..." << endl;
}

void Assistant::display()
{
	Student::display();

	cout << "Assistant's display..." << endl;

	cout << "Salary: " << salary << endl;
	cout << endl;
}

int main()
{
	Assistant a1("Ali Uyanik", 3, 5000);
	Assistant a2("Mehmet Bilir", 3, 3000);

	Student *s1 = &a1;
	Student *s2 = &a2;

	s1->display();
	s2->display();

	Student *s3 = new Assistant("Ayse Ucar", 3, 2000);

	s3->display();
	s3->~Student();

	return 0;
}