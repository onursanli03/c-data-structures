#include <iostream>
#include <string>

using namespace std;

const int YEAR = 3;

class student{
	string name;
	double gpa;
	int sYear;
	int id;
	static int stID;
	static int stYear[YEAR];

public:
	student(string, double, int);
	static void printAllStudents();
	void print() const;
	int numOfStudents();
	double getGPA();
	int getYear();
	int getStdId();	

};



int main()
{


	return 0;
}

