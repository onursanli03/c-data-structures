#include<iostream>
#include<exception>

using namespace std;

class InvalidIndexException: public exception
{
	virtual const char* what() const throw()
	{
		return "Invalid Index error occurred...";
	}

}indexError;

int main()
{
	int number[]={1, -3, 5, -7, 9, -11, 13, -15, 17, -19};

	int index;

	try{
		cout << "Enter an index value: ";
		cin >> index;

		if((index >= 10) || (index < 0))
			throw indexError;

		cout << "Number:" << number[index] << endl;
	}
	catch(exception& e)
	{
		cout << e.what() << endl;
	}

	return 0;
}

