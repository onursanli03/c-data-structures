#include<iostream>
#include <math.h>

using namespace std;

int getNumber(int index) throw (int)
{
	int number[]={1, -3, 5, -7, 9, -11, 13, -15, 17, -19};

	if((index >= 10) || (index < 0))
		throw index;

	return number[index];
}

float getSquareRoot(int number) throw (const char*)
{
	if(number < 0)
		throw "Negative number";

	return sqrt((float)number);

}

int main()
{
	int index;

	cout << "Enter an index value: ";
	cin >> index;
	
	try{
		int number = getNumber(index);
		cout << "Number: " << number << endl;
		cout << "Square root: " << getSquareRoot(number) << endl;
	}
	catch(const int i)        //index error
	{
		cout << "Invalid index value: " << i << endl;
		
	}
	catch(const char* error)       //Negative number error
	{
		cout << error << endl; 
	}

	cout << "Continues..." << endl;

	return 0;
}

