#include <iostream>
#include "partTimeEmployee.h"

using namespace std;

void partTimeEmployee::set(string first, string last, long id,
	double rate, double hours)
{
	setName(first, last);
	setId(id);
	payRate = rate;
	hoursWorked = hours;
}

void partTimeEmployee::setPayRate(double rate)
{
	payRate = rate;
}

double partTimeEmployee::getPayRate()
{
	return payRate;
}

void partTimeEmployee::setHoursWorked(double hours)
{
	hoursWorked = hours;
}

double partTimeEmployee::getHoursWorked()
{
	return hoursWorked;
}

