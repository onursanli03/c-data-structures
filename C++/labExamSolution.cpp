#include <iostream>
#include <string.h>

using namespace std;

const int TYPE = 3;

class Vehicle{
	static const char *status[TYPE]; // 0- New , 1-Second Hand , 2-Rented
protected:
	char  * brand;        // Name of producer eg., Opel, Mercedes
	int   year;           //year of product
	int   speed;          //max speed
	int   statusCode;     // valid codes; 0, 1, 2
public:
	Vehicle(char * _brand, int _year = 2015, int _speed = 200, int _statusCode = 0);
	Vehicle(const Vehicle & _vec); // copy constructor
	virtual ~Vehicle();
	void setBrand(char * _brand);
	virtual void display() const;
};

const char *Vehicle::status[] = { "New", "SecondHand", "Rented" };

Vehicle::Vehicle(char * _brand, int _year, int _speed, int _statusCode){

	brand = new char[strlen(_brand) + 1];
	strcpy(brand, _brand);
	year = _year;
	speed = _speed;
	statusCode = _statusCode;

}
Vehicle::Vehicle(const Vehicle &d){

	brand = new char[strlen(d.brand) + 1];
	strcpy(brand, d.brand);
	year = d.year;
	speed = d.speed;
	statusCode = d.statusCode;

}

Vehicle::~Vehicle(){
	cout << "Vehicle destructor..\n";
	delete[] brand;

}

void Vehicle::setBrand(char *_brand){
	delete[] brand;
	brand = new char[strlen(_brand) + 1];
	strcpy(brand, _brand);
}

void Vehicle::display()const{

	cout << "= Vehicle info = " << endl;
	cout << "Year     : " << year << endl;
	cout << "Max Speed: " << speed << endl;
	cout << "Brand    : " << brand << endl;
	cout << "Status   : " << status[statusCode] << endl;

}


class Car : public Vehicle{
	bool   airCond;  //air conditioner?
	short  carType; // 0:Family, 1:sports, 2:race
	char * owner;   //owner name
public:
	Car(char * _brand, char * _owner, int _year = 2014, int _speed = 240, int _statusCode = 0, bool airCond = false, short carType = 0);
	Car(const Car & _car);
	~Car();
	void setOwner(char * _owner);
	void setConditioner(bool _airCond);
	void setType(short _carType);
	void display() const;
	Car & operator =(const Car & _vec);
	bool operator ==(const Car & _c);
};

Car::Car(char * _brand, char * _owner, int _year, int _speed, int _statusCode, bool _airCond, short _carType)
	:Vehicle(_brand, _year, _speed, _statusCode){

	owner = new char[strlen(_owner) + 1];
	strcpy(owner, _owner);
	airCond = _airCond;
	carType = _carType;
}
Car::Car(const Car &d) :Vehicle(d){
	owner = new char[strlen(d.owner) + 1];
	strcpy(owner, d.owner);
	airCond = d.airCond;
	carType = d.carType;
}
Car::~Car(){
	cout << "Car destructor..\n";
	delete[] owner;
}
void Car::setConditioner(bool _airCond){

	airCond = _airCond;

}
void Car::setType(short _carType){

	carType = _carType;
}
void Car::setOwner(char *_owner){
	delete[] owner;
	owner = new char[strlen(_owner) + 1];
	strcpy(owner, _owner);

}

Car & Car::operator=(const Car & d){

	if (this != &d){

		delete[] brand;
		brand = new char[strlen(d.brand) + 1];
		strcpy(brand, d.brand);

		delete[] owner;
		owner = new char[strlen(d.owner) + 1];
		strcpy(owner, d.owner);

		year = d.year;
		speed = d.speed;
		statusCode = d.statusCode;
		airCond = d.airCond;
		carType = d.carType;
	}
	return *this;
}
void Car::display()const{

	Vehicle::display();

	cout << "Owner    : " << owner << endl;

	cout << "AirCond  : ";
	if (airCond)
		cout << "Air Contitioned\n";
	else
		cout << "No Air Condition\n";

	cout << "Car Type : ";
	switch (carType){
	case 0:cout << "Family\n"; break;
	case 1:cout << "Sport\n"; break;
	case 2:cout << "Race\n"; break;

	}
	cout << endl;
}

bool Car::operator ==(const Car & _c)
{
	return ((year == _c.year) && (speed == _c.speed) && (statusCode == _c.statusCode) && !(strcmp(brand, _c.brand) && (airCond == _c.airCond) && (carType == _c.carType) && !(strcmp(owner, _c.owner))));
}


int main(){

	Vehicle *car1 = new Car("Audi", "James Hetfield", 2013, 250, 1, true, 0);
	Vehicle *car2 = new Car("Opel", "Kurt Cobain", 1992, 220, 2, true, 0);
	Vehicle *car3 = new Car("BMW", "Chris Cornell", 1974, 200, 1, false, 1);


	car1->display();
	car2->display();
	car3->display();

	cout << "CAR1 is being assigned to the CAR3..." << endl;
	cout << endl;

	car3 = car1;

	if (car1 == car3)     // Equality operator
		cout << "CAR1 and CAR3 are equal..!\n\n";
	else
		cout << "CAR1 and CAR3 are NOT equal..!\n\n";


	return 0;
}