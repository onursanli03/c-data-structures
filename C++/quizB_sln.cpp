#include <iostream>
#include <string.h>

using namespace std;

const int TYPE=3;

class Vehicle{
  static const char *status[TYPE]; // 1- New , 2-Second Hand , 3-Rented
protected:
  char  * brand;
  char  * owner;
  int   year;           //year of product
  int   speed;          //max speed
  int   statusCode;  
public:
  Vehicle(char * _brand, char * _owner, int _year=2014,int _speed=50,int _statusCode=0);
  Vehicle(const Vehicle & _vec); // copy constructor
  virtual ~Vehicle();
  void setBrand(char * _brand);
  void setOwner(char * _owner);
  Vehicle & operator =(const Vehicle & _vec);
  bool operator ==(const Vehicle & _v);
  virtual void display() const;
};

const char *Vehicle::status[]={"New","SecondHand","Rented"};

Vehicle::Vehicle(char * _brad, char * _owner ,int _year,int _speed,int _statusCode){
    brand=new char[strlen(_brad)+1];
    strcpy(brand,_brad);
    owner=new char[strlen(_owner)+1];
    strcpy(owner,_owner);
    year=_year;
    speed=_speed;
    statusCode=_statusCode;
    
  }
  Vehicle::Vehicle(const Vehicle &d){
    brand=new char[strlen(d.brand)+1];
    strcpy(brand,d.brand);
    owner=new char[strlen(d.owner)+1];
    strcpy(owner,d.owner);
    year=d.year;
    speed=d.speed;
    statusCode=d.statusCode;
    
  }

  bool Vehicle::operator ==(const Vehicle &d){
    
	return (!(strcmp(owner,d.owner))&&!(strcmp(brand,d.brand))&&(year==d.year)&&(speed==d.speed)&&(statusCode==d.statusCode));
   
  }
  Vehicle::~Vehicle(){
	cout << "Vehicle destructor..\n";
    delete [] brand;
    delete [] owner;
    
  }
  void Vehicle::setOwner(char *_owner){
    delete [] owner;
    owner=new char[strlen(_owner)+1];
    strcpy(owner,_owner);
    
  }
  void Vehicle::setBrand(char *_brand){
    delete [] brand;
    brand=new char[strlen(_brand)+1];
    strcpy(brand,_brand);
  }
  Vehicle & Vehicle::operator=(const Vehicle & d){
   
	  if(this !=&d){
        delete [] owner;
         delete [] brand;
         brand=new char[strlen(d.brand)+1];
         strcpy(brand,d.brand);
         owner=new char[strlen(d.owner)+1];
         strcpy(owner,d.owner);
         year=d.year;
         speed=d.speed;
         statusCode=d.statusCode;
           }
	   return *this;
  }
   void Vehicle::display()const{
    cout <<"Brand: "<<brand<<endl;
    cout <<"Owner: "<<owner<<endl;
    cout <<"Year : "<<year<<endl;
    cout <<"Speed: "<<speed<<endl;
    cout <<"Status: "<<status[statusCode]<<endl;
	cout << endl;
  }


class Car: public Vehicle{
  bool   airCond;  //air conditioner?
  short   carType; // 0:Family, 1:sports, 2:race
public:
  Car(char * _brand, char * _owner, int _year=2014,int _speed=50,int _statusCode=0, bool airCond=false,short carType=0);
  Car(const Car & _car);
  ~Car();
  void setConditioner(bool _airCond);
  void setType(short _carType);
  void display() const;
};

  Car::Car(char * _brand,char * _owner ,int _year,int _speed,int _statusCode,bool _airCond,short _carType)
	  :Vehicle(_brand,_owner,_year,_speed,_statusCode){
       
	  airCond = _airCond;
      carType = _carType;
  }
  Car::Car(const Car &d):Vehicle(d){
    airCond = d.airCond;
    carType = d.carType;
  }
  Car::~Car(){
    cout << "Car destructor..\n";
  }
  void Car::setConditioner(bool _airCond){
    
	   airCond=_airCond;
    
  }
  void Car::setType(short _carType){
    
	  carType=_carType;
  }
  void Car::display()const{
    
	  Vehicle::display();
    
      cout<<"AirCond: ";
       if(airCond)
         cout<<"Air Contitioned Car..\n";
      else
	     cout<<"No Air Condition..\n";
    
    cout <<"Car Type: ";
    switch(carType){
      case 0:cout <<"Family\n";break;
      case 1:cout<<"Sport\n";break;
      case 2:cout<<"Race\n";break;
    }
  }
  

int main(){
  
  Vehicle v1("Audi","James Hetfield", 2013, 250, 2);  
  Vehicle v2("Opel", "Kurt Cobain", 1992, 220, 1);

  v1.display();
  v2.display();

  v2=v1;  //Assignment operator
 
  cout << "After Assignment now..\n";
  if(v1==v2)     // Equality operator
	  cout << "Vehicles are equal...\n\n";
  else
     cout << "Vehicles are NOT equal..!\n\n";

    
  Vehicle *car=new Car("Opel","Hasan Bilir", 1994, 150, 2, true, 2);
  car->display();

  car->~Vehicle();

  return 0;
}
