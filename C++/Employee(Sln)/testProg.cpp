#include <iostream>
#include "partTimeEmployee.h"
#include "fullTimeEmployee.h"
     
int main()
{
    fullTimeEmployee newEmp("John", "Smith", 75, 56000, 5700);
    partTimeEmployee tempEmp("Bill", "Nielson", 275, 15.50, 57);

	employeeType *e1;
	employeeType *e2;


   // newEmp.print();
    cout << endl;
   // tempEmp.print();

	e1 = &newEmp;
	e2 = &tempEmp;

	e1->print();
	e2->print();

    return 0;
}