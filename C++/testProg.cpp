
#include <iostream>
#include "ptrMemberVarType.h"
 
using namespace std;
  

int main()
{
    ptrMemberVarType listOne;                      //Line 1

    int num;                                       //Line 2
    int index;                                     //Line 3

    cout << "Line 4: Enter 5 integers." << endl;   //Line 4

    for (index = 0; index < 5; index++)            //Line 5
    {
        cin >> num;                                //Line 6
        listOne.insertAt(index, num);              //Line 7
    }

    cout << "Line 8: listOne: ";                   //Line 8
    listOne.print();                               //Line 9
    cout << endl;                                  //Line 10

       //Declare listTwo and initialize it using listOne
    ptrMemberVarType listTwo(listOne);             //Line 11

    cout << "Line 12: listTwo: ";                  //Line 12
    listTwo.print();                               //Line 13
    cout << endl;                                  //Line 14

    listTwo.insertAt(5, 34);                       //Line 15
    listTwo.insertAt(2, -76);                      //Line 16

    cout << "Line 17: After modifying listTwo: ";  //Line 17
    listTwo.print();                               //Line 18
    cout << endl;                                  //Line 19

    cout << "Line 20: After modifying listTwo, " 
         << "listOne: ";                           //Line 20
    listOne.print();                               //Line 21
    cout << endl;                                  //Line 22
   
   

    return 0;                                      //Line 28
}

