#include<iostream>
#include<string>

#include "employee.h"

using namespace std;


employee::employee()
{
	name = "";
	numOfServiceYears = 0;
	pay = 0.0;
}

employee::employee(string s, int n, double p)
{
	name = s;
	numOfServiceYears = n;
	pay = p;
}

void employee::print() const
{
	cout << "Name: " << name << endl;
	cout << "Number of service years: " << numOfServiceYears << endl;
	cout << "Salary: $" << pay << endl;
}


void employee::updatePay(double x)
{
	pay = pay + x;
}

int employee::getNumOfServiceYears() const
{
	return numOfServiceYears;
}

double employee::getPay() const
{
	return pay;
}

