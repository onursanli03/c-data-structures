#include <iostream>
#include <iomanip>
#include "employee.h"

using namespace std;

int main()
{
	employee newEmpl1;
	employee newEmpl2("John Harris", 0, 25000);
	employee newEmpl3("Samantha", 15, 56900);

	cout << fixed << showpoint << setprecision(2);

	newEmpl1.print();
	newEmpl2.print();
	newEmpl3.print();

	return 0;
}//end main