#include <string>

using namespace std;

class employee{

	string name;
	int numOfServiceYears;
	double pay;

public:
	employee();
	employee(string, int, double);

	int getNumOfServiceYears() const;
	double getPay() const;
	void print() const;
	void updatePay(double);


};