#include <iostream>

using namespace std;

class clockType
{
	friend ostream& operator<<(ostream&, const clockType&);
	friend istream& operator>>(istream&, clockType&);

public:
	void setTime(int hours, int minutes, int seconds);
	//Function to set the member variables hr, min, and sec.  
	//Postcondition: hr = hours; min = minutes; sec = seconds

	void getTime(int& hours, int& minutes, int& seconds) const;
	//Function to return the time.
	//Postcondition: hours = hr; minutes = min; seconds = sec

	clockType operator++();
	//Overload the pre-increment operator.
	//Postcondition: The time is incremented by one second.

	clockType operator++(int x);

	bool operator==(const clockType& otherClock) const;
	//Overload the equality operator.
	//Postcondition: Returns true if the time of this clock 
	//               is equal to the time of otherClock,
	//               otherwise it returns false.

	bool operator!=(const clockType& otherClock) const;
	//Overload the not equal operator.
	//Postcondition: Returns true if the time of this clock 
	//               is not equal to the time of otherClock,
	//               otherwise it returns false.

	bool operator<=(const clockType& otherClock) const;
	//Overload the less than or equal to operator.
	//Postcondition: Returns true if the time of this clock
	//               is less than or equal to the time of 
	//               otherClock, otherwise it returns false.

	bool operator<(const clockType& otherClock) const;
	//Overload the less than operator.
	//Postcondition: Returns true if the time of this clock
	//               is less than the time of otherClock,
	//               otherwise it returns false.

	bool operator>=(const clockType& otherClock) const;
	//Overload the greater than or equal to operator.
	//Postcondition: Returns true if the time of this clock
	//               is greater than or equal to the time of 
	//               otherClock, otherwise it returns false.

	bool operator>(const clockType& otherClock) const;
	//Overload the greater than operator.
	//Postcondition: Returns true if the time of this clock
	//               is greater than the time of otherClock,
	//               otherwise it returns false.

	clockType(int hours = 0, int minutes = 0, int seconds = 0);
	//Constructor to initialize the object with the values 
	//specified by the user. If no values are specified,
	//the default values are assumed.
	//Postcondition: hr = hours; min = minutes; 
	//               sec = seconds;


private:
	int hr;  //variable to store the hours
	int min; //variable to store the minutes
	int sec; //variable to store the seconds
};

//Overload the pre-increment operator.
clockType clockType::operator++()
{
	sec++;                  //Step a

	if (sec > 59)           //Step b
	{
		sec = 0;            //Step b.1
		min++;              //Step b.2

		if (min > 59)       //Step b.3
		{
			min = 0;        //Step b.3.1
			hr++;           //Step b.3.2

			if (hr > 23)    //Step b.3.3
				hr = 0;     //Step b.3.3.1
		}
	}

	return *this;           //Step c
}

//Overload the post-increment operator.
clockType clockType::operator++(int x)
{
	clockType temp = *this;

	sec++;
	if (sec > 59)
	{
		sec = 0;
		min++;
		if (min > 59)
		{
			min = 0;
			hr++;

			if (hr > 23)
				hr = 0;
		}
	}

	return temp;   //return the old value of the object
}

//Overload the equality operator.
bool clockType::operator==(const clockType& otherClock) const
{
	return (hr == otherClock.hr && min == otherClock.min
		&& sec == otherClock.sec);
}

//Overload the not equal operator.
bool clockType::operator!=(const clockType& otherClock) const
{
	return (hr != otherClock.hr || min != otherClock.min
		|| sec != otherClock.sec);
}

//Overload the less than or equal to operator.
bool clockType::operator<=(const clockType& otherClock) const
{
	return ((hr < otherClock.hr) ||
		(hr == otherClock.hr && min < otherClock.min) ||
		(hr == otherClock.hr && min == otherClock.min &&
		sec <= otherClock.sec));
}

//Overload the less than operator.
bool clockType::operator<(const clockType& otherClock) const
{
	return ((hr < otherClock.hr) ||
		(hr == otherClock.hr && min < otherClock.min) ||
		(hr == otherClock.hr && min == otherClock.min &&
		sec < otherClock.sec));
}

//Overload the greater than or equal to operator.
bool clockType::operator>=(const clockType& otherClock) const
{
	return ((hr > otherClock.hr) ||
		(hr == otherClock.hr && min > otherClock.min) ||
		(hr == otherClock.hr && min == otherClock.min &&
		sec >= otherClock.sec));
}

//Overload the greater than operator.
bool clockType::operator>(const clockType& otherClock) const
{
	return ((hr > otherClock.hr) ||
		(hr == otherClock.hr && min > otherClock.min) ||
		(hr == otherClock.hr && min == otherClock.min &&
		sec > otherClock.sec));
}

void clockType::setTime(int hours, int minutes, int seconds)
{
	if (0 <= hours && hours < 24)
		hr = hours;
	else
		hr = 0;

	if (0 <= minutes && minutes < 60)
		min = minutes;
	else
		min = 0;

	if (0 <= seconds && seconds < 60)
		sec = seconds;
	else
		sec = 0;
}

void clockType::getTime(int& hours, int& minutes,
	int& seconds) const
{
	hours = hr;
	minutes = min;
	seconds = sec;
}

//Constructor
clockType::clockType(int hours, int minutes, int seconds)
{
	setTime(hours, minutes, seconds);
}

//Overload the stream insertion operator.
ostream& operator<<(ostream& osObject, const clockType& timeOut)
{
	if (timeOut.hr < 10)
		osObject << '0';
	osObject << timeOut.hr << ':';

	if (timeOut.min < 10)
		osObject << '0';
	osObject << timeOut.min << ':';

	if (timeOut.sec < 10)
		osObject << '0';
	osObject << timeOut.sec;

	return osObject;  //return the ostream object
}

//overload the stream extraction operator
istream& operator>> (istream& is, clockType& timeIn)
{
	char ch;

	is >> timeIn.hr;                          //Step a

	if (timeIn.hr < 0 || timeIn.hr >= 24)     //Step a
		timeIn.hr = 0;

	is.get(ch);           //Read and discard :. Step b

	is >> timeIn.min;                         //Step c

	if (timeIn.min < 0 || timeIn.min >= 60)   //Step c
		timeIn.min = 0;

	is.get(ch);           //Read and discard :. Step d

	is >> timeIn.sec;                         //Step e

	if (timeIn.sec < 0 || timeIn.sec >= 60)   //Step e
		timeIn.sec = 0;

	return is;                                //Step f
}


int main()
{
	clockType myClock(5, 6, 23);                     //Line 1
	clockType yourClock;                             //Line 2

	cout << "Line 3: myClock = " << myClock
		<< endl;                                    //Line 3
	cout << "Line 4: yourClock = " << yourClock
		<< endl;                                    //Line 4

	cout << "Line 5: Enter the time in the form "
		<< "hr:min:sec ";                           //Line 5
	cin >> myClock;                                  //Line 6
	cout << endl;                                    //Line 7

	cout << "Line 8: The new time of myClock = "
		<< myClock << endl;                         //Line 8

	++myClock;                                       //Line 9

	cout << "Line 10: After incrementing the time, "
		<< "myClock = " << myClock << endl;         //Line 10

	yourClock.setTime(13, 35, 38);                   //Line 11
	cout << "Line 12: After setting the time, "
		<< "yourClock = " << yourClock << endl;     //Line 12

	if (myClock == yourClock)                        //Line 13
		cout << "Line 14: The times of myClock and "
		<< "yourClock are equal." << endl;      //Line 14
	else                                             //Line 15
		cout << "Line 16: The times of myClock and "
		<< "yourClock are not equal." << endl;  //Line 16

	if (myClock <= yourClock)                        //Line 17
		cout << "Line 18: The time of myClock is "
		<< "less than or equal to " << endl
		<< "the time of yourClock." << endl;    //Line 18
	else                                             //Line 19
		cout << "Line 20: The time of myClock is "
		<< "greater than the time of "
		<< "yourClock." << endl;                //Line 20

	yourClock = myClock++;                              //Line 21

	cout << "Line 22: myClock = " << myClock << endl;   //Line 22
	cout << "Line 23: yourClock = " << yourClock
		<< endl;

	return 0;
}