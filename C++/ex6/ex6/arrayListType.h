#ifndef H_arrayListType 
#define H_arrayListType
 
template <class elemType>
class arrayListType 
{
public:
    const arrayListType<elemType>& 
                operator=(const arrayListType<elemType>&);
		//overload the assignment operator

    bool isEmpty() const;
	   //Returns true if the list is empty; 
 	   //otherwise, returns false
    bool isFull() const;
	   //Returns true if the list is full; 
 	   //otherwise, returns false
    int listSize()  const;
	   //Returns the size of the list, that is, the number 
	   //of elements currently in the list
    int maxListSize()  const;
	   //Returns the maximum size of the list, that is, the 
	   //maximum number of elements that can be stored in 
 	   //the list
    void print() const;
	   //Outputs the elements of the list
    bool isItemAtEqual(int location, const elemType& item);
	   //If the item is same as the list element at position
	   //specified by location, returns true;
	   //otherwise returns false
    void insertAt(int location, const elemType& insertItem);
	   //Insert an item in the list at the specified location.
	   //The item to be inserted and the location are passed 
       //as parameter to the function.
	   //If the list is full or location is out of range,
	   //an appropriate message is displayed.
    void insertEnd(const elemType& insertItem);
	   //Insert an item at the end of the list. The parameter 
	   //insertItem specifies the item to be inserted.
	   //If the list is full, an appropriate message is 
	   //displayed.
    void removeAt(int location);
	   //Remove the item from the list at the position  
	   //specified by location. If location is out of range, 
	   //an appropriate message is printed.
    void retrieveAt(int location, elemType& retItem);
	   //Retrieve the element from the list at the  
       //position specified by location. The item is 
       //returned via the parameter retItem.
	   //If location is out of range, an appropriate 
	   //message is printed.
    void replaceAt(int location, const elemType& repItem);
	   //Replace the elements in the list at the position 
       //specified by location. The item to be replaced is  
 	   //specified by the parameter repItem.
	   //If location is out of range, an appropriate 
	   //message is printed.
    void clearList();
	   //All elements from the list are removed. After this 
	   //operation the size of the list is zero.
    int seqSearch(const elemType& item) const;
  	   //Searches the list for a given item. 
	   //If item is found, returns the location  in the array
	   //where the item is found; otherwise returns -1.
    void insert(const elemType& insertItem);
	   //The item specified by the parameter insertItem is 
       //inserted at the end of the list. However, first the
       //list is searched to see if the item to be inserted is 
       //already in the list. If the item is already in the list
       //or the list is full, an appropriate message is output.
    void remove(const elemType& removeItem);
       //Remove an item from the list. The parameter removeItem 
	   //specifies the item to be removed.
    arrayListType(int size = 100);
       //Constructor
       //Creates an array of size specified by the parameter
       //size. The default array size is 100.
    arrayListType (const arrayListType<elemType>& otherList); 
       //Copy constructor
    ~arrayListType();
	   //Destructor
	   //Deallocate the memory occupied by the array.

protected:
    elemType *list; //array to hold the list elements
    int length;	    //to store the length of the list
    int maxSize;	//to store the maximum size of the list
};


template <class elemType>
bool arrayListType<elemType>::isEmpty() const
{
    return (length == 0);
}

template <class elemType>
bool arrayListType<elemType>::isFull() const
{
    return (length == maxSize);
}

template <class elemType>
int arrayListType<elemType>::listSize()  const
{
    return length;
}

template <class elemType>
int arrayListType<elemType>::maxListSize()  const
{
	return maxSize;
}

template <class elemType>
void arrayListType<elemType>::print() const
{
	int i;

	for (i = 0; i < length; i++)
		cout<<list[i]<<" ";
	cout<<endl;
}

template <class elemType>
bool arrayListType<elemType>::isItemAtEqual
                   (int location, const elemType& item)
{
	return (list[location] == item);
}

template <class elemType>
void arrayListType<elemType>::insertAt
                   (int location, const elemType& insertItem)
{
	int  i;

	if (location < 0 || location >= maxSize)
		cout<<"The position of the item to be inserted "
			<<"is out of range"<<endl;
	else
		if (length >= maxSize)  //list is full
			cout<<"Cannot insert in a full list"<<endl;
		else
		{
			for (i = length; i > location; i--)
				list[i] = list[i - 1];	//move the elements down

			list[location] = insertItem;	//insert the item at the 
 										//specified position

			length++;	//increment the length
		}
} //end insertAt


template <class elemType>
void arrayListType<elemType>::insertEnd(const elemType& insertItem)
{
	if (length >= maxSize)  //the list is full
		cout<<"Cannot insert in a full list"<<endl;
	else
	{
		list[length] = insertItem;	//insert the item at the end
		length++;	//increment length
	}
} //end insertEnd

template <class elemType>
void arrayListType<elemType>::removeAt(int location)
{
	int i;

	if (location < 0 || location >= length)
    	cout<<"The location of the item to be removed "
			<<"is out of range"<<endl;
	else
	{
   		for (i = location; i < length - 1; i++)
	 		list[i] = list[i+1];

		length--;
	}
} //end removeAt

template <class elemType>
void arrayListType<elemType>::retrieveAt
                     (int location, elemType& retItem)
{
	if (location < 0 || location >= length)
    	cout<<"The location of the item to be retrieved is "
			<<"out of range"<<endl;
	else
		retItem = list[location];
} // retrieveAt

template <class elemType>
void arrayListType<elemType>::replaceAt
                    (int location, const elemType& repItem)
{
	if (location < 0 || location >= length)
    	cout<<"The location of the item to be replaced is "
			<<"out of range"<<endl;
	else
		list[location] = repItem;

} //end replaceAt

template <class elemType>
void arrayListType<elemType>::clearList()
{
	length = 0;
} // end clearList

template <class elemType>
int arrayListType<elemType>::seqSearch(const elemType& item) const
{
	int loc;
	bool found = false;

	for (loc = 0; loc < length; loc++)
	   if (list[loc] == item)
	   {
			found = true;
			break;
	   }

	if(found)
		return loc;
	else
		return -1;
} //end seqSearch

template <class elemType>
void arrayListType<elemType>::insert(const elemType& insertItem)
{
	int loc;

	if (length == 0)					 //list is empty
		list[length++] = insertItem; //insert the item and 
 									 //increment the length
	else
		if (length == maxSize)
			cout<<"Cannot insert in a full list."<<endl;
		else
		{
			loc = seqSearch(insertItem);

			if (loc == -1)   //the item to be inserted 
							//does not exist in the list
				list[length++] = insertItem;
			else
				cout<<"the item to be inserted is already in "
 					<<"the list. No duplicates are allowed."<<endl;
	}
} //end insert

template <class elemType>
void arrayListType<elemType>::remove(const elemType& removeItem)
{
	int loc;

	if (length == 0)
		cout<<"Cannot delete from an empty list."<<endl;
	else
	{
		loc = seqSearch(removeItem);

		if (loc != -1)
			removeAt(loc);
		else
			cout<<"The tem to be deleted is not in the list."
				<<endl;
	}

} //end remove


template <class elemType>
arrayListType<elemType>::arrayListType(int size)
{
	if (size < 0)
	{
 		cout<<"The array size must be positive. Creating "
 			<<"an array of size 100. "<<endl;

 		maxSize = 100;
 	}
 	else
 	   maxSize = size;

	length = 0;
	list = new elemType[maxSize];
}

template <class elemType>
arrayListType<elemType>::~arrayListType()
{
	delete [] list;
}

	//copy constructor
template <class elemType>
arrayListType<elemType>::arrayListType
                  (const arrayListType<elemType>& otherList)
{
	int j;

	maxSize = otherList.maxSize;
	length = otherList.length;
	list = new elemType[maxSize]; 	//create the array

	if (length != 0) 				//if otherList is not empty
		for (j = 0; j < length; j++)  //copy otherList
 			list [j] = otherList.list[j];
}//end copy constructor

template <class elemType>
const arrayListType<elemType>& arrayListType<elemType>::operator=
			(const arrayListType<elemType>& otherList)
{
	if (this != &otherList)	//avoid self-assignment
	{
		if (maxSize != otherList.maxSize)
			cout<<"Cannot copy. The two arrays are of "
				<<"different sizes"<<endl;
		else
		{
			int j;

			length = otherList.length;

			if (length != 0) 	//if otherList is not empty
				for (j = 0; j < length; j++)  //copy otherList
 					list[j] = otherList.list[j];
		}
	}

	return *this;
}

#endif
