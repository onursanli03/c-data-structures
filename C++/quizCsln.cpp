#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

const int MAXOS = 5;

class Computer{
	static const char *OSnames[MAXOS]; // 1- Windows , 2-Unix , 3 Linux , 4-Pardus, 5-MOS
protected:
	char  * brand;
	int   PSnumber; // how many processor?
	int   memory;
	int   HD;
	int   OScode;
public:
	Computer(char * _brand, int _memory, int _HD, int _PSnumber = 1, int _OScode = 0);
	Computer(const Computer & _c); // copy constructor
	virtual ~Computer();
	void setBrand(char * _brand)
	{
		delete[] brand;
		brand = new char[strlen(_brand) + 1];
		strcpy(brand, _brand);
	}
	Computer & operator =(const Computer & _c);
	virtual void display() const;
};

const char *Computer::OSnames[] = { "Windows", "Unix", "Linux", "Pardus", "MOS" };

Computer::Computer(char * _brand, int _memory, int _HD, int _PSnumber, int _OScode) :
memory(_memory), PSnumber(_PSnumber), OScode(_OScode), HD(_HD)
{
	cout << "Computer: constructor()" << endl;
	brand = new char[strlen(_brand) + 1];
	strcpy(brand, _brand);
}

Computer::Computer(const Computer & _c)
{
	cout << "Computer: Copy constructor()" << endl;
	this->brand = new char[strlen(_c.brand) + 1];
	strcpy(this->brand, _c.brand);
	OScode = _c.OScode;
	memory = _c.memory;
	PSnumber = _c.PSnumber;
	HD = _c.HD;
}

Computer::~Computer()
{
	cout << "Computer: destructor()" << endl;
	delete[] brand;
}

Computer & Computer::operator =(const Computer & _c)
{
	cout << "Computer: = operator()" << endl;
	if (this != &_c){
		cout << "computer op= started" << endl;
		delete[] this->brand; // delete old resource
		this->brand = new char[strlen(_c.brand) + 1];    // assign new resource
		strcpy(this->brand, _c.brand);
		OScode = _c.OScode;
		memory = _c.memory;
		PSnumber = _c.PSnumber;
		HD = _c.HD;
		cout << "computer op= worked" << endl;
	}
	return *this;
}

void Computer::display() const
{
	cout << "Brand: " << brand << endl;
	cout << "Memory  is " << memory << " KB." << endl;
	cout << "Hard disc is " << HD << " GB." << endl;
	cout << "It has " << PSnumber << " processors." << endl;
	cout << "Operating System: " << OSnames[OScode] << endl;
}

class PC : public Computer{
	bool   DVD;
	bool   TD; // Tape driver
public:
	PC(char * _brand, int _memory, int _HD, int _PSnumber = 1, int _OScode = 0);
	PC(const PC & _pc);
	~PC();
	
	void setDVD(bool _DVD)
	{
		DVD = _DVD;
	};
	void setTD(bool _TD)
	{
		TD = _TD;
	};
	void display() const;
};

PC::PC(char * _brand, int _memory, int _HD, int _PSnumber, int _OScode)
	:Computer(_brand, _memory, _HD, _PSnumber, _OScode)
{
	cout << "PC: constructor()" << endl;
	DVD = false;
	TD = false;
}

// Copy constructor
PC::PC(const PC & _pc) :Computer(_pc)// call base class copy constructor
{
	DVD = _pc.DVD;
	TD = _pc.TD;
}

PC::~PC()
{
	cout << "PC: destructor()" << endl;
}


void PC::display() const
{
	Computer(*this).display();

	if (DVD)
		cout << "It has got a DVD" << endl;
	else
		cout << "It hasn't got a DVD" << endl;
	if (TD)
		cout << "It has got a TD" << endl;
	else
		cout << "It hasn't got a TD" << endl;
}

int main()
{
	Computer c1("Casper",2048,160);
	c1.display();

	Computer c2=c1; // copy constructor

	c2.display();

	c2.setBrand("IBM");

	c1=c2;  // assignment operator
	c1.display();

	/*Test Polymorphism */

	/*Computer *c;
	PC p3("HP", 4096, 200);

	c = &p3;

	c->display();*/

	

	system("PAUSE");
	return 0;
}
