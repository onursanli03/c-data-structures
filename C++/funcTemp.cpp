#include <iostream>

using namespace std;

template< typename T>
void printArray(const T *arr, int count)
{
	for (int i = 0; i < count; i++)
		cout << arr[i] << " ";

	cout << endl;
 
}

int main()
{
	const int ACOUNT = 5;
	const int BCOUNT = 7;
	const int CCOUNT = 6;

	int a[ACOUNT] = { 1, 2, 3, 4, 5 };
	double b[BCOUNT] = { 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7 };
	char c[CCOUNT] = "HELLO";


	cout << "Array a contains:" << endl;
	//call integer function-template specialization
	printArray( a, ACOUNT );

	cout << "Array b contains:" << endl;
	//call double function-template specialization
	printArray( b, ACOUNT );

	cout << "Array c contains:" << endl;
	//call character function-template specialization
	printArray( c, ACOUNT );
	
	return 0;
}