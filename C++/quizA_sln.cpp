#include <iostream>
#include <string.h>

using namespace std;

const int CTYPE=3;

class Clock{
	static const char *CTypes[CTYPE];

protected:
	char *brand;
	double price;
	int clockType;

public:
	Clock(char * _brand, double _price, int _clockType=0);
	Clock(const Clock & _c);
	virtual ~Clock();
	void setBrand(char * _brand);
	void setPrice(double _price);
	Clock & operator =(const Clock & _c);
	bool operator==(const Clock & _c);
	virtual void display() const;

};

const char *Clock::CTypes[] = {"Digital","Mechanic","Solar"};

Clock::Clock(char * _brand, double _price, int _clockType)
{
	brand=new char[strlen(_brand)+1];
    strcpy(brand,_brand);
	price=_price;
	clockType=_clockType;
}

Clock::Clock(const Clock & _c)
{
	brand=new char[strlen(_c.brand)+1];
    strcpy(brand,_c.brand);
	price=_c.price;
	clockType=_c.clockType;
}

Clock::~Clock()
{
	cout << "Clock Destructor...\n";
    delete [] brand;
}

void Clock::setBrand(char * _brand)
{
	delete [] brand;
	brand=new char[strlen(_brand)+1];
	strcpy(brand,_brand);
}


void Clock::setPrice(double _price)
{
	price=_price;
}


bool Clock::operator==(const Clock &  _c) 
{
	return(strcmp(brand,_c.brand)==0 && price==_c.price && clockType==_c.clockType);
}


Clock& Clock::operator=(const Clock & _c) 
{
	if(this != &_c){
		delete [] brand;
		brand=new char[strlen(_c.brand)+1];
		strcpy(brand,_c.brand);
		price=_c.price;
		clockType=_c.clockType;
	}

	return *this;
}


void Clock::display() const
{
	cout << "\nClock's Display..." << endl;
	cout << "Clock Type : " << CTypes[clockType] << endl;
	cout << "Brand :" << brand << endl;
	cout << "Price : " << price << endl;
}

class ArmClock: public Clock{

	char * owner;

public:
	ArmClock(char * _brand, double _price, int _clockType=0, char *_owner="");
	ArmClock(const ArmClock & _ac);
	~ArmClock();
	void setOwner(char * _owner);
	void display () const;

};


ArmClock::ArmClock(char * _brand, double _price, int _clockType, char *_owner):Clock(_brand, _price, _clockType)
{
	owner=new char[strlen(_owner)+1];
    strcpy(owner,_owner);
}

ArmClock::ArmClock(const ArmClock & _ac):Clock(_ac)
{
	owner=new char[strlen(_ac.owner)+1];
    strcpy(owner,_ac.owner);
}

ArmClock::~ArmClock()
{
	cout << "ArmClock Destructor...\n";
    delete [] owner;
}

void ArmClock::setOwner(char * _owner)
{
	delete [] owner;
	owner=new char[strlen(_owner)+1];
	strcpy(owner,_owner);
}

void ArmClock::display() const {
	Clock::display();
	cout << "Owner : " << owner << endl;
	cout << endl;
}

int main()
{
    
	Clock c1("Swatch", 300.00, 1);
	Clock c2("Casio", 200.00, 0);
	ArmClock c3("Diesel", 500.00, 1, "Orcun");

    c1.display();
    
    c1 = c2;
	c1.display();
		
	if(c1==c2)
	  cout << "\nWatches are equal..\n";
	else
	  cout << "\nWatches are NOT equal..!\n";
	  
	
	c3.setBrand("Fossil");
	c3.setPrice(750.00);
	c3.setOwner("Chris");
	
	c3.display();
		
	cout << "\n::Test Polymorphism::";
	Clock *dClock = new ArmClock("Tissot", 400.00, 2, "Ali");
        
    dClock->display();
        
	dClock->~Clock();  //delete c; check with/without virtual destructor

	return 0;

}
