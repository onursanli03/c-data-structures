//
// Created by ghost on 31.10.2015.
//

#include <iostream>
#include "PersonType.h"

using namespace std;

void PersonType::print() const
{
    cout << fName << " " << lName;
}

void PersonType::setName(string First, string Last)
{
    fName = First;
    lName = Last;
}

string PersonType::getfName() const
{
    return fName;
}

string PersonType::getlName() const
{
    return lName;
}

PersonType::PersonType(string First, string Last)
{
    fName = First;
    lName = Last;
}