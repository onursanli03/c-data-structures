//
// Created by ghost on 31.10.2015.
//

#include <iostream>
#include "DateType.h"

using namespace std;

void DateType::setDate(int Month, int Day, int Year)
{
    nMnth = Month;
    nDay = Day;
    nYr = Year;
}

int DateType::getDay() const
{
    return nDay;
}

int DateType::getMonth() const
{
    return nMnth;
}

int DateType::getYear() const
{
    return nYr;
}

void DateType::printDate() const
{
    cout << nMnth << "-" << nDay << "-" << nYr;
}