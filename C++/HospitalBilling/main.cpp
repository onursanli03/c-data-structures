#include<iostream>
#include <string>

#include "PersonType.h"
#include "DoctorType.h"
#include "PatientType.h"
#include "BillType.h"

using namespace std;

int main()
{
    PatientType Patient;
    DoctorType  Doctor;
    PersonType Person;
    BillType Bill(3,500,1000,5,5);
    DateType Date;

    Patient.setPinfo();
    Doctor.setDoctor();
    Patient.print();
    Doctor.printAtt();
    Patient.setAdmtDt(1,4,1993);
    Patient.setDcDt(3,4,1993);
    Bill.print();
    Bill.getT(500,500,500);

    return 0;
}