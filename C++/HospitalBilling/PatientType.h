//
// Created by ghost on 31.10.2015.
//

#ifndef HOSPITALBILLING_PATIENTTYPE_H
#define HOSPITALBILLING_PATIENTTYPE_H


#include<string>
#include "PersonType.h"
#include "DateType.h"
#include "DoctorType.h"

using namespace std;

class PatientType :public PersonType, DateType
{
private:
    int PtID;
    DateType dtBth;
    DateType admtDt;
    DateType DcDt;
    DoctorType Dr;
    string pFirst;
    string pLast;
    PersonType FullName;

public:
    PatientType(string First="", string Last="", int PtID = 0,int Month = 0,int Day = 0, int Year = 0000);


    void print()const;

    int getPtID()const;

    string getPfirst()const;
    string getPlast()const;
    void setPinfo()const;

    void setAdmtDt(int Month, int Day, int Year);
    void setDcDt(int Month, int Day, int Year);

};
#endif //HOSPITALBILLING_PATIENTTYPE_H
