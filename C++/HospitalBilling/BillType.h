//
// Created by ghost on 31.10.2015.
//

#ifndef HOSPITALBILLING_BILLTYPE_H
#define HOSPITALBILLING_BILLTYPE_H
#include <string>
using namespace std;
class BillType
{
private :

    double T, pC, dF, rF;
    int PtID;

public:
    void print()const;
    double getDFee();
    double getPCharge();
    double getRFee();
    int getPid();
    double getT(double Room, double Pharmacy, double Doctor);
    BillType(int ID, double patC,double docF, double roomF, double totalF);
};
#endif //HOSPITALBILLING_BILLTYPE_H
