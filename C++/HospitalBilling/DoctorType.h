//
// Created by ghost on 31.10.2015.
//

#ifndef HOSPITALBILLING_DOCTORTYPE_H
#define HOSPITALBILLING_DOCTORTYPE_H

#include<iostream>
#include<string>
#include "PersonType.h"
using namespace std;

//inherits from personType class
class DoctorType : public PersonType
{
private :
    string dSpec;
    string dFirst;
    string dLast;
public:

    //public member function of doctor class
    void printAtt()const;
    DoctorType(string First = "", string Last = "", string Spec = "");
    string getDspec();
    string getDFirst()const;
    string getDlast()const;
    string getDoctor();
    void setDoctor();
    void setDoctorSpec(string Spec);

};


#endif //HOSPITALBILLING_DOCTORTYPE_H
