//
// Created by ghost on 31.10.2015.
//

#ifndef HOSPITALBILLING_DATETYPE_H
#define HOSPITALBILLING_DATETYPE_H

#include <string>
using namespace std;
class DateType
{
public:
    void setDate(int Month, int Day, int Year);

    int getDay() const;

    int getMonth() const;

    int getYear() const;

    void printDate() const;

    DateType(int Month = 1, int Day = 1, int Year = 1900);

private:
    int nMnth;
    int nDay;
    int nYr;
};
#endif //HOSPITALBILLING_DATETYPE_H
