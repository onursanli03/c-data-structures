//
// Created by ghost on 31.10.2015.
//

#ifndef HOSPITALBILLING_PERSONTYPE_H
#define HOSPITALBILLING_PERSONTYPE_H

#include <string>

using namespace std;

class PersonType
{
public:
    void print() const;

    void setName(string First, string Last);

    string getfName() const;

    string getlName() const;

    PersonType(string First = "", string Last = "");

protected:
    string fName;
    string lName;
};
#endif //HOSPITALBILLING_PERSONTYPE_H
