//
// Created by ghost on 31.10.2015.
//
#include <iostream>
#include "DoctorType.h"


using namespace std;

DoctorType::DoctorType(string First, string Last ,string Spec)
{
    dSpec = Spec;
}
string DoctorType::getDoctor()
{
    return dSpec;
}
string DoctorType::getDFirst() const
{
    return PersonType::getfName();
}
string DoctorType::getDlast()const
{
    return PersonType::getlName();
}
void DoctorType::printAtt() const
{
    cout << "Attending Physician";
    PersonType::print();
    cout << " " << dSpec << endl;
}
void DoctorType::setDoctor()
{
    cout << "Enter Doctor's First Name : ";
    cin >> dFirst;
    cout << "Enter Doctor's Last Name : ";
    cin >> dLast;
    cout << "Enter Doctor's Speciality : ";
    cin >> dSpec;

    PersonType::setName(dFirst,dLast);
}
void DoctorType::setDoctorSpec(string Spec)
{
    dSpec = Spec;
}