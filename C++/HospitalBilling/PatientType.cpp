//
// Created by ghost on 31.10.2015.
//

#include <iostream>
#include "PatientType.h"


using namespace std;

PatientType::PatientType(string First, string Last, int ID, int Month, int Day, int Year):FullName(First,Last),dtBth(Month,Day,Year)
{

    PtID = ID;

}
void PatientType::print() const
{


    cout << "Patient: ";
    FullName.print();
    cout << endl;


}
string PatientType::getPfirst()const
{
    return FullName.getfName();
}
string PatientType::getPlast()const
{
    return FullName.getlName();
}


void PatientType::setPinfo()const
{
    cout << "Enter patient's ID: ";
    cin >> PtID;
    cout << "Enter patient's first name: ";
    cin >> pFirst;
    cout << "Enter patient's last name: ";
    cin >> pLast;

}

void PatientType::setAdmtDt( int Month,int Day, int Year)
{
    admtDt.setDate(Month,Day,Year);
    cout << "Admint Date";
    admtDt.printDate();

}

void PatientType::setDcDt(int Month,int Day, int Year)
{
    DcDt.setDate(Month,Day,Year);
    cout << "Discharge Date";
    DcDt.printDate();

}

int PatientType::getPtID()const
{
    return PtID;
}