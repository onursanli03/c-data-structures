#include <iostream>
using namespace std;

class Base
{
public:
	Base(){cout<<"Constructing Base"<<endl;}
	virtual ~Base(){cout<<"Destroying Base"<<endl;}
};

class Derive:public Base
{
public:
	Derive(){cout<<"Constructing Derive"<<endl;}
	~Derive(){cout<<"Destructing Derive"<<endl;}
};

void main()
{
	Base *basePtr = new Derive();
	delete basePtr;
	system("PAUSE");
}