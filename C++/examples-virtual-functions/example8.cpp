#include <iostream>
using namespace std;
class Base
{
public:
	virtual void show() = 0;
};

class Derived:public Base
{
public:
	void show(){cout<<"Implementation of virtual function"<<endl;}
};

int main()
{
//	Base obj;    //compile time error (cannot instantiate abstract classes)
	Base *b;
	Derived d;
	b= &d;
	b->show();
	d.show();
	system("PAUSE");
	return 0;
}