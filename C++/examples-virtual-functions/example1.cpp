#include <iostream>
#include <string>
using namespace std;

class petType
{
private:
	string name;
public:
	void print();
	petType(string n="");
};

class dogType: public petType
{
private:
	string breed;
public:
	void print();
	dogType(string n="", string b="");
};

void petType::print()
{
	cout <<"Name "<<name;
}

petType::petType(string n)
{
	name = n;
}

dogType::dogType(string n, string b)
	:petType(n)
{
	breed = b;	
}

void dogType::print()
{
	petType::print();
	cout<<", Breed: "<< breed <<endl;
}

//function which has a formal parameter of the base class
void callPrint(petType &p)
{
	p.print();
}

int main(void)
{
	petType pet("Lucky");
	dogType dog("Tommy", "German Shepperd");

	pet.print();
	cout<<endl;
	dog.print();

	cout<<"***Calling the function callPrint***"<<endl;
	callPrint(pet);
	cout<<endl;
	callPrint(dog);
	cout<<endl;
	system("PAUSE");
	return 0;
}
