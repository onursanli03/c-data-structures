#include <iostream>
#include <string>
using namespace std;

class petType
{
private:
	string name;
public:
	virtual void print();
	petType(string n="");
};

class dogType: public petType
{
private:
	string breed;
public:
	void print();
	dogType(string n="", string b="");
	void setBreed(string s);
};

void petType::print()
{
	cout <<"Name "<<name;
}

petType::petType(string n)
{
	name = n;
}

dogType::dogType(string n, string b)
	:petType(n)
{
	breed = b;	
}

void dogType::print()
{
	petType::print();
	cout<<", Breed: "<< breed <<endl;
}

void dogType::setBreed(string s)
{
	breed = s;
}

//function which has a formal parameter of the base class
void callPrint(petType &p)
{
	p.print();
}

int main(void)
{
	petType *pet;
	dogType *dog;

	dog = new dogType("Tommy", "German Shepperd");
	dog->setBreed("Siberian Husky");

	pet= dog;

	pet->print();
//	system("PAUSE");
	return 0;
}
