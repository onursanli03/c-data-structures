#include <iostream>
#include <string>


using namespace std;

//It?s used to create an deep copy of the passed object, instead of creating a shallow copy.
//With a shallow copy, you?d create a reference to the values of the original object,
//as show in the following code:


class Student{
  int  stdid;
  char * name;
public:
  Student(int _stdid,char * _name):stdid(_stdid)
  {
    name=new char[strlen(_name)+1];
    strcpy(name,_name);
  }


  // shallow  copy constructor
/*  Student(const Student & _object)
  {
    *this=_object;
  }

*/
  // deep  copy constructor
  Student(Student & _object)
  {
    stdid=_object.stdid;
    name=new char[strlen(_object.name)+1];
    strcpy(name,_object.name);
  }

  ~Student()
  {
    delete [] name;
  }

  void setId(int _stdid)
  {
    stdid=_stdid;
  }

  void setName(char * _name)
  {
  	delete [] name;
    name=new char[strlen(_name)+1];
    strcpy(name,_name);
  }

  void display() const
  {
    cout << "Id   : " << stdid   << endl;
    cout << "Name : " << name << endl;
  }
};


int main()
{
      Student s1(1,"ahmet kucuk");

      Student s2=s1;

      s1.display();
      s2.display();

      s2.setName("mehmet bilir");
      s1.display();
      s2.display();
       

      system("PAUSE");
      return 0;
}