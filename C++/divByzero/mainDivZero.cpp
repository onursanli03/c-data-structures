
#include <iostream>
#include <iomanip>
#include "divisionByZero.h"

using namespace std;

int main()
{
	double num1, num2;
	char opr;
	bool done = false;

	cout << fixed << showpoint << setprecision(2);

	cout << "Enter two numbers: ";
	cin >> num1 >> num2;
	cout << endl;

	cout << "Enter operator: + (addition), - (subtraction),"
		<< " * (multiplication), / (division): ";
	cin >> opr;
	cout << endl;

	cout << num1 << " " << opr << " " << num2 << " = ";

	switch (opr)
	{
	case '+':
		cout << num1 + num2 << endl;
		break;
	case '-':
		cout << num1 - num2 << endl;
		break;
	case '*':
		cout << num1 * num2 << endl;
		break;
	case '/':

		do
		{
			try
			{
				if (num2 == 0)
					throw divisionByZero();

				cout << num1 / num2 << endl;
				done = true;
			}
			catch (divisionByZero divByZeroObj)
			{
				cout << divByZeroObj.what() << endl;
				cin.clear();
				cin.ignore(100, '\n');
				cout << "Enter the divisor: ";
				cin >> num2;
				cout << endl;

				cout << num1 << " " << opr << " " << num2 << " = ";
			}
		} while (!done);

		break;
	default:
		cout << "Illegal operation" << endl;
	}

	return 0;
}
