#include "circularLinkedList.h"

int main() {


	circularLinkedList<int> list;

	int num;

	cout << "Enter integers ending "
		<< "with -999" << endl;
	cin >> num;

	while (num != -999)
	{
		list.insertNode(num);
		cin >> num;
	}

	cout << endl;

	cout << "list : " << endl;

	list.print();

	cout << endl;

	list.insertFirst(9);
	cout << "insert first!!" << endl;
	list.print();
	cout << endl;
	cout <<"----------------------------------------"<< endl;

	
	list.insertLast(10);
	cout << "insert last!!" << endl;
	list.print();
	cout << endl;
	cout << "----------------------------------------" << endl;


	list.deleteFirst();
	cout << "deleted first!!" << endl;
	list.print();
	cout << endl;
	cout << "----------------------------------------" << endl;

	list.deleteLast();
	cout << "deleted last!!" << endl;
	list.print();
	cout << endl;
	cout << "----------------------------------------" << endl;

	system("PAUSE");
	getchar();
	return 0;
}