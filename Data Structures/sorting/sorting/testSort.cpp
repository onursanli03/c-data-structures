#include <iostream>
#include "SSorting.h"


using namespace std;

int * setArray(int *a, int size)
{
	for (int i = 0; i<size; i += 5) {
		a[i] = i * 10 + 5;
		a[i + 1] = i * 1 + 4;
		a[i + 2] = i * 2 + 3;
		a[i + 3] = i * 3 + 2;
		a[i + 4] = i * 4 + 1;
	}
	return a;
}


int * setSortedArray(int *a, int size)
{
	for (int i = 0; i<size; i++) {
		a[i] = i;
	}
	return a;
}


void printListInfo(const int list[], int length)
{
	for (int i = 0; i < length; i++)
	{
		cout << list[i] << " ";
		if ((i + 1) % 15 == 0)
			cout << endl;
	}

	cout << endl;
}


int main()
{
	//TEST1 Number of comparisons

	SSorting<int> s;

/*	int *a;
	int size = 1024;

	 a = new int[size];
	 
	 int num;

	 cout << " Enter a number to search: ";
	 cin >> num;

	 int comparisons = 0, loc;

	 setArray(a, size);

	 loc = s.binarySearch(a, size, comparisons, num);

	 cout << "****Binary Search****" << endl;


	 if (loc != -1) {
		 cout << "Item found at" << loc
			 << "\n \tNumber of comparisons"
			 << comparisons << endl;
	 }
	 else {
		 cout << "\n\t" << num << " is NOT in the list"
			 << " \n\tNumber of Comparisons = "
			 << comparisons << endl;
	 }

	 loc = s.seqSearch(a, size, comparisons, num);

	 cout << "****Sequantion Search****" << endl;

		 if (loc != -1) {
			 cout << "Item found at" << loc
				 << "\n \tNumber of comparisons"
				 << comparisons << endl;
		 }
		 else {
			 cout << "\n\t" << num << " is NOT in the list"
				 << " \n\tNumber of Comparisons = "
				 << comparisons << endl;
		 } */

		 int *b;
		 int d = 50000;
		 
		 b = new int[d];

		 s.bubbleSort(setArray(b, d), d);
		 s.selectionSort(setArray(b, d), d);
		 s.insertionSort(setArray(b, d), d);
		 s.quickSort(setArray(b, d), d);


	system("PAUSE");
	return 0;
}