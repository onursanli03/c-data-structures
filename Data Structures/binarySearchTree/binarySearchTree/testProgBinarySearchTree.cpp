#include <iostream>
#include "binarySearchTree.h"

using namespace std;

int main()
{
	bSearchTreeType<int> tree1;
	bSearchTreeType<int> tree2;

	int arr[10] = { 50, 30, 83, 14, 41, 57, 104, 38, 44, 5 };

	for (int i = 0; i < 10; i++)
		tree1.insert(arr[i]);

	tree2 = tree1;

	cout << "Height of Tree1: " << tree1.treeHeight() << endl;

	cout << endl;

	cout << "Preorder Traversal: ";
	tree1.preorderTraversal();
	cout << endl;

	cout << "Postorder Traversal: ";
	tree1.postorderTraversal();
	cout << endl;

	cout << "Inorder Traversal: ";
	tree1.inorderTraversal();
	cout << endl;

	cout << endl;

	tree2.insert(47);
	cout << "Height of Tree2: " << tree2.treeHeight() << endl;

	cout << "Number of nodes in Tree1: " << tree1.treeNodeCount() << endl;
	cout << "Number of nodes in Tree2: " << tree2.treeNodeCount() << endl;

	cout << endl;

	cout << "Number of leaves in Tree1: " << tree1.treeLeavesCount() << endl;
	cout << "Number of leaves in Tree2: " << tree2.treeLeavesCount() << endl;

	cout << endl;

	tree1.insert(200);
	tree1.insert(45);

	cout << "\nNumber of single Parents in Tree1 : " << tree1.singleParent() << endl;

	cout << "\nNumber of single Parents in Tree2 : " << tree2.singleParent() << endl;


	tree1.swapSubtrees();
	
	cout << endl;

	tree1.inorderTraversal();

	cout << endl;

	cout << "\nNumber of double Parents in Tree1 : " << tree1.doubleParent() << endl;

	cout << "\nNumber of double Parents in Tree2 : " << tree2.doubleParent() << endl;

	tree1.insertPostorder(tree2);

	system("PAUSE");
	return 0;
}