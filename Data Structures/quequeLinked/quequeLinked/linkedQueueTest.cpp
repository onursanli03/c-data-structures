//Test Program linked queue 

#include <iostream>
#include "queueAsArray.h"

using namespace std;


int main()
{


	queueType<int> q1(100);


	int num;

	cout << "Enter integers ending "
		<< "with -999" << endl;
	cin >> num;

	while (num != -999)
	{
		q1.addQueue(num);
		cin >> num;
	}

	cout << endl;
	while (!q1.isEmptyQueue())
	{
		cout << q1.front()<< " " << endl;
		q1.deleteQueue();
	}


	system("PAUSE");
	return 0;
}